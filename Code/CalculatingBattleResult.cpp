#include "CalculatingBattleResult.h"
#include <fstream>

CalculatingBattleResult::CalculatingBattleResult()
    : comparerFunction{ nullptr }, CountOfCountries{}, Countries{}, ArmyOfCountry{}
{};
CalculatingBattleResult::~CalculatingBattleResult() = default;
//I added this function before the test. It was not possible to compile, I did not check. If it doesn't work, comment out the function and its use. Will work.
void writeWinnerToFile(std::string outputFile, std::string country1, std::string country2)
{
    ofstream writeFlow(outputFile� std::ios::binary);

    std::string country = winner(country1, country2);

    writeFlow << country << std::endl;
    writeFlow << countryOfArmy[country].first << std::endl;

    for (size_t i = 0; i < countryOfArmy[country].first; i++)
    {
        for (size_t j = 0; j < NUMBER_OF_COLUMN; j++)
        {
            writeFlow << countryOfArmy[country].second[i][j] << " ";
        }
        std::cout << std::endl;
    }

    writeFlow.close();
}

void CalculatingBattleResult::getMatrixFromFile(std::string statisticsFileName)
{
    std::ifstream fightInformationFlow(statisticsFileName);
    if (!fightInformationFlow)
    {
        std::cout << "Can not open file." << std::endl;
        return;
    }
    std::string line;
    std::getline(fightInformationFlow, line);
    CountOfCountries = std::stoi(line);

    Countries = new std::string[CountOfCountries];
    ArmyOfCountry.reserve(CountOfCountries);

    for (size_t i = 0; i < CountOfCountries; i++)
    {
        std::getline(fightInformationFlow, line);
        Countries[i] = line;
        std::getline(fightInformationFlow, line);
        ArmyOfCountry[Countries[i]].first = std::stoi(line);
        ArmyOfCountry[Countries[i]].second = new int* [std::stoi(line)];
        for (size_t j = 0; j < ArmyOfCountry[Countries[i]].first; j++)
        {
            std::getline(fightInformationFlow, line);
            ArmyOfCountry[Countries[i]].second[j] = getIntArrayFromLine(line);
        }
    }
}

double CalculatingBattleResult::calculateCountryPower(std::string country)
{
    if (ArmyOfCountry.count(country) == 0)
    {
        std::cout << "Not find country: " << country << std::endl;
        return -1;
    }
    double power{};
    for (size_t i = 0; i < ArmyOfCountry[country].first; i++)
    {
        power += calculateArmyPower(ArmyOfCountry[country].second[i]);
    }
    return power;
}

void CalculatingBattleResult::Output()
{
    for (size_t i = 0; i < CountOfCountries; i++)
    {
        int** matrix = ArmyOfCountry[Countries[i]].second;
        std::cout << "Country: " << Countries[i] << std::endl;
        std::cout << "Count of Armies of this Country: " << ArmyOfCountry[Countries[i]].first << std::endl;
        for (size_t j = 0; j < ArmyOfCountry[Countries[i]].first; j++)
        {
            for (size_t k = 0; k < NUMBER_OF_COLUMNS; k++)
            {
                std::cout << matrix[j][k] << ' ';
            }
            std::cout << std::endl;
        }
        std::cout << std::endl;
    }
}

std::string CalculatingBattleResult::winner(std::string country1, std::string country2)
{
    if (ArmyOfCountry.count(country1) == 0 || ArmyOfCountry.count(country2) == 0)
    {
        std::cout << "One of two country or above don't find " << std::endl;
        return std::string{};
    }
    if (comparerFunction == nullptr)
        return calculateCountryPower(country1) > calculateCountryPower(country2) ? country1 : country2;
    else
        return comparerFunction(calculateCountryPower(country1), calculateCountryPower(country2)) ? country1 : country2;

}

void CalculatingBattleResult::getFunction(comparer comparerFunction)
{
    this->comparerFunction = comparerFunction;
}

int* CalculatingBattleResult::getIntArrayFromLine(std::string line)
{
    int* numbersFromLine = new int[NUMBER_OF_COLUMNS];

    std::string number{};
    int index{};
    for(int i{ 0 }; i < line.size(); i++)
    {
        if ( line[i] == ' ')
        {
            numbersFromLine[index++] = std::stoi(number);
            number.clear();
        }
        else
            number += line[i];
    }
    if (!number.empty())
        numbersFromLine[index] = std::stoi(number);

    return numbersFromLine;
}

double CalculatingBattleResult::calculateArmyPower(int* army)
{
    return (army[1] * POWER_OF_INFANTRY +
            army[2] * POWER_OF_CAVALRY +
            army[3] * POWER_OF_ARTILLERY) *
            ((double)army[4] * army[5] / 10000);
}