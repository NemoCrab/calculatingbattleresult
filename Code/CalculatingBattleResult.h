#ifndef CALCULATING_BATTLE_RESULT_H
#define CALCULATING_BATTLE_RESULT_H
#include <string>
#include <unordered_map>
#include <iostream>

using comparer = bool(*)(int, int); 

class CalculatingBattleResult
{
    public:
        CalculatingBattleResult();
        ~CalculatingBattleResult();
        void getMatrixFromFile(std::string);
        double calculateCountryPower(std::string);
        void writeWinnerToFile(std::string, std::string);
        void Output();
        std::string winner(std::string, std::string);
        void getFunction(comparer comparerFunction);
    private:
        const char LINE_SEPARATOR{ ' ' };
        const int NUMBER_OF_COLUMNS{ 6 };
        const int POWER_OF_INFANTRY{ 10 };
        const int POWER_OF_CAVALRY{ 20 };
        const int POWER_OF_ARTILLERY{ 30 };
        int CountOfCountries;
        std::string* Countries;
        std::unordered_map<std::string, std::pair<int, int**>> ArmyOfCountry;
        comparer comparerFunction;
        int* getIntArrayFromLine(std::string);
        double calculateArmyPower(int*);
};

#endif // CALCULATING_BATTLE_RESULT_H