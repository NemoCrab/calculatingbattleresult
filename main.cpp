﻿#include "CalculatingBattleResult.h"

bool comparerFunction(int a, int b)
{
    return a > b;
}

int main()
{
    CalculatingBattleResult myFirstBattle;
    myFirstBattle.getMatrixFromFile("FightArmyStatistics.txt");
    myFirstBattle.Output();
    myFirstBattle.getFunction(comparerFunction);
    std::cout << myFirstBattle.winner("USSR", "Germany") << std::endl;
    std::cout << myFirstBattle.calculateCountryPower("USSR");
    myFirstBattle.writeWinnerToFile("ResultOfBattle", "Germany", "USSR");
    return 0;
}